Списки доступа (Access List)

![img](https://gitlab.com/evgeny_v_v/cpt/-/raw/master/img/15.jpg)

Рисунок  – Схема сети, для работы с access list

Первым делом необходимо настроить NAT, сделав акцент на access list. Настройка осуществляется в роутере. 

*conf t*

*int fa0/0*

*ip nat outside*

*exit*

*int range fa0/1.2*

*ip nat inside*

*exit*

*int range fa0/1.3*

*ip nat inside*

*exit*

*int range fa0/1.4*

*ip nat inside*

*exit*

*ip access-list standart FOR-NAT*(создаём стандартный, потому что в данном случае надо указать только сеть источник)

*permit 192.168.2.0 0.0.0.255*

*permit 192.168.3.0 0.0.0.255*

*permit 192.168.4.0 0.0.0.255*

*exit*

*ip nat inside source list FOR-NAT interface fa0/0 overload*

*end*

Проверить доступ компьютера к сети интернет, проверить доступ сервера к интернету. Так как в access list мы не включили сервер, то и доступа к интернету у него не будет.

Чтобы ограничить доступ к внутренней сети используем access list.

В роутере который подключён к сети Интернет мы будем использовать расширенные access list

*conf t*

*ip access-list extended FROM-OUTSIDE*

*deny ip any 192.168.2.0 0.0.0.255*

*deny ip any 192.168.3.0 0.0.0.255*

*deny ip any 192.168.4.0 0.0.0.255*

*deny ip any 192.168.5.0 0.0.0.255*

*exit*

*int fa0/0*

*ip access-group FROM-OUTSIDE in*

*end*

*wr mem*

В данном случае интернет перестанет работать на всех устройствах, чтобы этого не было необходимо создать разрешающее правило.

*conf t*

*ip access-list extended FROM-OUTSIDE*

*permit ip any host 210.210.0.2*

*end*

*wr mem*

В данном случае интернет появится.

Чтобы сократить количество access list пропишем следующие команды, удалив и заново создав новый access list.

*conf t*

*no ip access-list extended FROM-OUTSIDE*

*ip access-list extended FROM-OUTSIDE*

*permit ip any host 210.210.0.2*

*end* 

Если в сети есть удалённый доступ, то нужно сделать следующее:

*conf t*

*username admin privilege 15 password cisco*

*enable password cisco*

*line vty 0 4*

*login LOCAL*

*end*

*wr mem*

Попробуем удалённый доступ к роутеру с внешнего сервера.

*telnet 210.210.0.2* доступ будет к нашей сети, что не очень хорошо, следовательно мы должны запретить доступ по telnet к нашей сети.

*conf t*

*ip access-list extended FROM-OUTSIDE*

*deny tcp any host 210.210.0.2 eq telnet*

*end*

*wr mem*

Но доступ всё равно будет, потому что разрешающее правило будет над запрещающим, в этом можно убедиться, набрав команду *show run*

Поэтому проще всего удалить и заново создать access list в той последовательности, что нам нужно.

*conf t*

*no ip access-list extended FROM-OUTSIDE*

*ip access-list extended FROM-OUTSIDE*

*deny tcp any host 210.210.0.2 eq telnet*

*permit ip any host 210.210.0.2*

*end*

*wr mem*

В данном случае уже доступ к локальной сети, с удалённого сервера, будет запрещён.

Теперь настроим чтобы к локальному серверу был доступ только у необходимой сети. Перейдём в настройки роутера.

*conf t*

*ip access-list standard TO-1C*

*permit 192.168.2.0 0.0.0.255*

*end*

*int fa0/1.5*

*ip access-group TO-1C out*

*end*

*wr mem*