 Динамическая маршрутизация EIGPR

![img](https://gitlab.com/evgeny_v_v/cpt/-/raw/master/img/14.jpg)

Рисунок  – Схема для динамической маршрутизации EIGPR

Пример из предыдущего урока.

Перейдём к настройке R1

*conf t*

*router eigrp 1*

*network 192.168.1.0 0.0.0.255*

*network 10.10.10.0 0.0.0.3*

*network 10.10.11.0 0.0.0.3*

*no auto-summary*

*end*

*wr mem*

То же самое делаем для других роутеров, более подробно можно посмотреть в предыдущем разделе в примере 1. Точно так же сделаем все проверки в том числе с отключением одного из линков.

Минус EIGRP в том, что он работает только с оборудованием Cisco.