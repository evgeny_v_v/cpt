Пример 1.

![img](https://gitlab.com/evgeny_v_v/cpt/-/raw/master/img/12.jpg)

Рисунок 1 – OSPF для первого примера

Настроим конфигурацию маршрутизатора R1.

*en*

*conf t*

*int loopback 0*

*ip address 192.168.100.1 255.255.255.255*

*no shutdown*

*exit*

*router ospf 1*

*network 192.168.1.0 0.0.0.255 area 0*

*network 10.10.10.0 0.0.0.3 area 0*

*network 10.10.11.0 0.0.0.3 area 0*

*end*

*wr mem*

После введения данных команд OSPF автоматически включается на всех интерфейсах которые соответствуют всем диапазонам адресов

Перейдём к настройке второго роутера R2.

*en*

*conf t*

*int loopback 0*

*ip address 192.168.100.2 255.255.255.255*

*no shutdown*

*exit*

*router ospf 1*

*network 192.168.2.0 0.0.0.255 area 0*

*network 10.10.10.0 0.0.0.3 area 0*

*network 10.10.12.0 0.0.0.3 area 0*

*end*

*wr mem*

Перейдём к настройке третьего роутера R3.

*en*

*conf t*

*int loopback 0*

*ip address 192.168.100.3 255.255.255.255*

*no shutdown*

*exit*

*router ospf 1*

*network 192.168.3.0 0.0.0.255 area 0*

*network 10.10.11.0 0.0.0.3 area 0*

*network 10.10.12.0 0.0.0.3 area 0*

*end*

*wr mem*

Теперь с помощью команды *show ip ospf neighbor* можем увидеть что роутеры нашли друг друга. Если пропишем команду *show ip route* тогда сможем увидеть ip адреса которые были прописаны с помощью ospf, они будут помечены буквой О. Как всегда, проверку любого соединения проверить стоит с помощью команды *ping*, чтобы понять, идут пакеты между нужными компьютерами/устройствами или нет. Данным соединением мы сделали не только автоматическую раздачу ip адресов, но и решили вопрос отказоустойчивости. Для этого можно поэкспериментировать с линками между роутерами. Делается это помощью команды i*nt gi0/1 &#10132; shutdown* (в зависимости от используемого интерфейса) тем самым у нас маршрут должен автоматически перестроится.

Пример 2.

![img](https://gitlab.com/evgeny_v_v/cpt/-/raw/master/img/13.jpg)

Рисунок 2 – OSPF для второго примера

Начнём с настройки loopback интерфейсов.

*conf t*

*int loopback 0*

*ip address 192.168.100.1 255.255.255.255*

*no shutdown*

*router ospf 1*

*network 192.168.10.0 0.0.0.255 area 0*

*network 192.168.20.0 0.0.0.255 area 0*

*network 192.168.1.0 0.0.0.3 area 0* (данными действиями мы анонсируем сеть, то есть маршрутизатору разрешаем посылать обновления по поводу данной сети)

*end*

*wr mem*

Для других роутеров настройка будет аналогична. 

*conf t*

*int loopback 0*

*ip address 192.168.100.2 255.255.255.255*

*no shutdown*

*router ospf 1*

*network 192.168.30.0 0.0.0.255 area 0*

*network 192.168.40.0 0.0.0.255 area 0*

*network 192.168.2.0 0.0.0.3 area 0*

*end*

*wr mem*

Для центрального роутера делаем тоже самое, только прописываем нужные ip адреса, которые мы задали ранее.

Для подключения локальной сети к сети Интернет пропишем всё необходимое на центральном роутере

*conf t*

*ip route 0.0.0.0 0.0.0.0 210.214.1.1*

*router ospf 1*

*default-information originate*

*end*

*wr mem*

Теперь с помощью технологии NAT настроим центральный маршрутизатор. Во-первых, определим какие интерфейсы у нас являются внешними, а какие внутренними.

*conf t*

*ip nat outside*

*int gi0/0*

*ip nat outside*

*exit*

*int gigabitEthernet 0/1 inside*

*exit*

*int gi 0/2*

*ip nat inside*

*exit*

*ip access-list standard FOR-NAT* 

Прописываем все ip адреса сетей, которым хотим дать доступ к сети Интернет:

*permit 192.168.10.0 0.0.0.255*

*permit 192.168.20.0 0.0.0.255*

*permit 192.168.30.0 0.0.0.255*

*permit 192.168.40.0 0.0.0.255*

*ip nat inside source list FOR-NAT interface gi0/0 overload*

*end*

*wr mem*

На данном примере мы настроили динамическую маршрутизацию, с помощью динамической маршрутизации 